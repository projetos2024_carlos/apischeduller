const connect = require("../db/connect");

module.exports = class scheduleController {

    static async createSchedule(req, res) {
        const {
            dateStart,
            dateEnd,
            days,
            user,
            classroom,
            timeStart,
            timeEnd,
        } = req.body;

        if (!dateStart || !dateEnd || !days || !user || !classroom || !timeStart || !timeEnd) {
            return res.status(400).json({ error: "Todos os campos devem ser preenchidos" });
        }''
        const daysString = days.map((day) => `${day}`).join(","); //Transformação do array para string e colocando , quando existem 2 ou mais objetos; Falta validação caso o dado recebido nao seja um array

        //Verificando caso nao haja uma reserva existente
        try {
            const overlapQuery = `SELECT * from schedule
            where classroom = '${classroom}'
            and (
                (dateStart<='${dateStart}' and dateEnd>='${dateEnd}')
            )and (
                (timeStart<='${timeStart}' and timeEnd>='${timeEnd}')
            )and (
                (days LIKE '%Seg%' and '${daysString}' like '%Seg%')OR
                (days LIKE '%Ter%' and '${daysString}' like '%Ter%')OR
                (days LIKE '%Qua%' and '${daysString}' like '%Qua%')OR
                (days LIKE '%Qui%' and '${daysString}' like '%Qui%')OR
                (days LIKE '%Sex%' and '${daysString}' like '%Sex%')OR
                (days LIKE '%Sab%' and '${daysString}' like '%Sab%')
            )
            
            `;

            connect.query(overlapQuery, function (err, result) {
                if (err) {
                    console.error(err);
                    return res.status(500).json({ error: "Erro ao verificar agendamento existente" });
                }

                //Se houver resultado na consulta, ja eiste agendamento

                if (result) {
                    return res.status(400).json({ error: "Sala já agendada nesse período" })
                }

                //Caso a query nao retorne nada, inserimos na tabela

                const insertQuery = `INSERT indo schedule (dateStart,
                    dateEnd,
                    days,
                    user,
                    classroom,
                    timeStart,
                    timeEnd) values(

                          '${dateStart}',
                          '${dateEnd}',
                          '${daysString}',
                          '${user}',
                          '${classroom}',
                          '${timeStart}',
                          '${timeEnd}',
                          )
                          `;
                          //Executando a query de inserção
                          connect.query(insertQuery, function (err) {
                            if (error) {
                                
                                return res.status(500).json({ error: "Erro ao cadastrar agendamento" });
                            }
                            return res.status(201).json({message: "Agendamento realizado com sucesso!"})
                          });
            })
        } catch (error) {
            console.error("Erro ao executar a consulta: ", error);
            res.status(500).json({error: "Erro interno do servidor"})
        }
    }

}